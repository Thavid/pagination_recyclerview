package com.example.thavid.testmvp;

import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TableLayout;
import android.widget.Toast;

import com.example.thavid.testmvp.fragment.Fragment1;
import com.example.thavid.testmvp.fragment.Fragment2;
import com.example.thavid.testmvp.fragment.Fragment3;

import java.util.ArrayList;

public class TapActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private NonSwipeableViewPager viewPager;
    private ArrayList<TabLayoutData> tabData;
    private ViewPagerAdapter pagerAdater;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tap);

        viewPager = (NonSwipeableViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        tabData = new ArrayList<>();
        tabData.add(new TabLayoutData(0, "Home", R.drawable.ic_cart_normal, Fragment1.newInstance()));
        tabData.add(new TabLayoutData(0, "Web Link", R.drawable.ic_cart_normal, new Fragment2()));
        tabData.add(new TabLayoutData(0, "Cart", R.drawable.ic_cart_normal, new Fragment3()));
        tabData.add(new TabLayoutData(0, "Store", R.drawable.ic_cart_normal, new Fragment2()));
        tabData.add(new TabLayoutData(0, "Shop", R.drawable.ic_cart_normal, new Fragment3()));


        pagerAdater = new ViewPagerAdapter(getSupportFragmentManager(), this, tabData);
        viewPager.setAdapter(pagerAdater);
        viewPager.setOffscreenPageLimit(5);

        tabLayout.setupWithViewPager(viewPager);
        pagerAdater.changeCustomeTab(tabLayout);

        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        pagerAdater.setTabIcon(tab.getPosition(), R.drawable.ic_cart_normal, Color.parseColor("#f57f22"), 0);
                        break;
                    case 1:
                        pagerAdater.setTabIcon(tab.getPosition(), R.drawable.ic_cart_normal, Color.parseColor("#f57f22"), 0);
                        break;
                    case 2:
                        pagerAdater.setTabIcon(tab.getPosition(), R.drawable.ic_cart_normal, Color.parseColor("#f57f22"), 0);
                        break;
                    case 3:
                        pagerAdater.setTabIcon(tab.getPosition(), R.drawable.ic_cart_normal, Color.parseColor("#f57f22"), 0);
                        break;
                    case 4:
                        pagerAdater.setTabIcon(tab.getPosition(), R.drawable.ic_cart_normal, Color.parseColor("#f57f22"), 0);
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        pagerAdater.setTabIcon(tab.getPosition(), R.drawable.ic_cart_normal, Color.parseColor("#1994d0"), 0);
                        break;
                    case 1:
                        pagerAdater.setTabIcon(tab.getPosition(), R.drawable.ic_cart_normal, Color.parseColor("#1994d0"), 0);
                        break;
                    case 2:
                        pagerAdater.setTabIcon(tab.getPosition(), R.drawable.ic_cart_normal, Color.parseColor("#1994d0"), 0);
                        break;
                    case 3:
                        pagerAdater.setTabIcon(tab.getPosition(), R.drawable.ic_cart_normal, Color.parseColor("#1994d0"), 0);
                        break;
                    case 4:
                        pagerAdater.setTabIcon(tab.getPosition(), R.drawable.ic_cart_normal, Color.parseColor("#1994d0"), 0);
                        break;
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
