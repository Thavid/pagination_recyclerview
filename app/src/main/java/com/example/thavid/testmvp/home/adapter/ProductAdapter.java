package com.example.thavid.testmvp.home.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.thavid.testmvp.R;
import com.example.thavid.testmvp.home.enity.ItemsPopular;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    private Context context;
    private List<ItemsPopular> listItemPopular;

    public ProductAdapter(Context context, List<ItemsPopular> listItemPopular) {
        this.context = context;
        this.listItemPopular = listItemPopular;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cartproduct,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
       ItemsPopular item = listItemPopular.get(i);
       if(!item.getImgPath().isEmpty()){
           Picasso.get().load(item.getImgPath()).into(viewHolder.imageView);
       }
       viewHolder.txtName.setText(item.getItemName());
       viewHolder.txtPrice.setText(item.getPrice()+"");
    }

    @Override
    public int getItemCount() {
        return listItemPopular.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView txtName,txtPrice;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imgProduct);
            txtName = itemView.findViewById(R.id.txtProductName);
            txtPrice = itemView.findViewById(R.id.txtPrice);
        }
    }
}
