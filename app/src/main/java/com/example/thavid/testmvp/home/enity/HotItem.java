package com.example.thavid.testmvp.home.enity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HotItem {

    private String createDate;
    private String level2;
    private String level3;
    private double priceDis;
    private String brand;
    private String level1;
    private int level3Id;
    private String model;
    private String isPublic;
    private String stockStatus;
    private int level1Id;
    private int level2Id;
    private String imgPath;
    private double price;
    private String size;
    private String itemName;
    private int disPer;
    private String description;
    private String itemCode;
    private String color;
    private int sysCode;

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getLevel2() {
        return level2;
    }

    public void setLevel2(String level2) {
        this.level2 = level2;
    }

    public String getLevel3() {
        return level3;
    }

    public void setLevel3(String level3) {
        this.level3 = level3;
    }

    public double getPriceDis() {
        return priceDis;
    }

    public void setPriceDis(double priceDis) {
        this.priceDis = priceDis;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getLevel1() {
        return level1;
    }

    public void setLevel1(String level1) {
        this.level1 = level1;
    }

    public int getLevel3Id() {
        return level3Id;
    }

    public void setLevel3Id(int level3Id) {
        this.level3Id = level3Id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(String isPublic) {
        this.isPublic = isPublic;
    }

    public String getStockStatus() {
        return stockStatus;
    }

    public void setStockStatus(String stockStatus) {
        this.stockStatus = stockStatus;
    }

    public int getLevel1Id() {
        return level1Id;
    }

    public void setLevel1Id(int level1Id) {
        this.level1Id = level1Id;
    }

    public int getLevel2Id() {
        return level2Id;
    }

    public void setLevel2Id(int level2Id) {
        this.level2Id = level2Id;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getDisPer() {
        return disPer;
    }

    public void setDisPer(int disPer) {
        this.disPer = disPer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getSysCode() {
        return sysCode;
    }

    public void setSysCode(int sysCode) {
        this.sysCode = sysCode;
    }

    @Override
    public String toString() {
        return "HotItem{" +
                "createDate='" + createDate + '\'' +
                ", level2='" + level2 + '\'' +
                ", level3='" + level3 + '\'' +
                ", priceDis=" + priceDis +
                ", brand='" + brand + '\'' +
                ", level1='" + level1 + '\'' +
                ", level3Id=" + level3Id +
                ", model='" + model + '\'' +
                ", isPublic='" + isPublic + '\'' +
                ", stockStatus='" + stockStatus + '\'' +
                ", level1Id=" + level1Id +
                ", level2Id=" + level2Id +
                ", imgPath='" + imgPath + '\'' +
                ", price=" + price +
                ", size='" + size + '\'' +
                ", itemName='" + itemName + '\'' +
                ", disPer=" + disPer +
                ", description='" + description + '\'' +
                ", itemCode='" + itemCode + '\'' +
                ", color='" + color + '\'' +
                ", sysCode=" + sysCode +
                '}';
    }
}
