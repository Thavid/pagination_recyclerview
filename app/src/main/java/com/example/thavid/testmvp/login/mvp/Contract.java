package com.example.thavid.testmvp.login.mvp;

import com.example.thavid.testmvp.Callback;

public interface Contract {

    public interface view{
        public void loading();
        public void hideLoading();
        public void loginSuccess();
        public void loginFail();
    }

    public interface presentor{
        public void login(String userName,String password);
    }

    public interface interactor{
        public void login(String userName, String password, Callback callback);
    }
}
