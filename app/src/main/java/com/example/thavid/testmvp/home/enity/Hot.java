package com.example.thavid.testmvp.home.enity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Hot {

    private int status;
    private List<HotItem> hotItem;
    private int totalPage;
    private String msg;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<HotItem> getHotItem() {
        return hotItem;
    }

    public void setHotItem(List<HotItem> hotItem) {
        this.hotItem = hotItem;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "Hot{" +
                "status=" + status +
                ", hotItem=" + hotItem +
                ", totalPage=" + totalPage +
                ", msg='" + msg + '\'' +
                '}';
    }
}
