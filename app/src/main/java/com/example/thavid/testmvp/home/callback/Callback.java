package com.example.thavid.testmvp.home.callback;

import com.example.thavid.testmvp.home.enity.Popular;

import retrofit2.Response;

public interface Callback {
    public void  onSuccess(Response<Popular> response);
    public void onFailed(String msg);


}
