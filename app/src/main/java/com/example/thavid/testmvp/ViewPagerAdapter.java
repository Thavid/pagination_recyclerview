package com.example.thavid.testmvp;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

class ViewPagerAdapter extends FragmentPagerAdapter {

    private Context context;
    private ArrayList<TabLayoutData> lstData;
    private TabLayout tabs;
    private ArrayList<View> views;

    public ViewPagerAdapter(FragmentManager fm,Context context,ArrayList<TabLayoutData> lstData) {
        super(fm);
        this.lstData = lstData;
        this.context = context;
        views = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int i) {
        return lstData.get(i).getFragment();
    }

    @Override
    public int getCount() {
        return lstData.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;

    }
    public void changeCustomeTab(TabLayout tabLayout) {
        tabs = tabLayout;
        for (int i = 0; i < lstData.size(); i++)
            tabs.getTabAt(i).setCustomView(getContentView(i));
    }

    public void setTabIcon(int position, int icon, int color, int num){
        tabs.getTabAt(position).setCustomView(setContentView(position, icon, color, num));
    }

    public View setContentView(int position, int icon, int color, int num){
        ImageView imageView = views.get(position).findViewById(R.id.tab_icon);
        imageView.setImageResource(icon);
        TextView txttitle = views.get(position).findViewById(R.id.txtTitle);
        txttitle.setTextColor(color);
        return views.get(position);
    }

    public View getContentView(int position){
        View view = LayoutInflater.from(context).inflate(R.layout.customtab, null);
        TextView txtTitle = view.findViewById(R.id.txtTitle);
        ImageView imageView = view.findViewById(R.id.tab_icon);
        txtTitle.setText(lstData.get(position).getTitle());
        imageView.setImageResource(lstData.get(position).getIcon());
        views.add(view);

        return view;
    }
}
