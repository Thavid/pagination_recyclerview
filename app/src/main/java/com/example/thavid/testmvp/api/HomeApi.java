package com.example.thavid.testmvp.api;

import com.example.thavid.testmvp.home.enity.Hot;
import com.example.thavid.testmvp.home.enity.Popular;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface HomeApi {
    @GET("api/mobile/home/list/{popularLimit}")
    Call<Popular> getPopular(@Path("popularLimit") int popularLimit);
    @GET("api/mobile/home/list/{hotNum}/{hotPage}")
    Call<Hot> getHot(@Path("hotNum") int hotNum, @Path("hotPage") int hotPage);
}
