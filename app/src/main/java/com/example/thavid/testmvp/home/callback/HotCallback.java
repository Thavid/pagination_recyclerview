package com.example.thavid.testmvp.home.callback;

import com.example.thavid.testmvp.home.enity.Hot;

import retrofit2.Response;

public interface HotCallback {
    public void getHotSuccess(Response<Hot> response);
    public void getHotFail(String msg);
}
