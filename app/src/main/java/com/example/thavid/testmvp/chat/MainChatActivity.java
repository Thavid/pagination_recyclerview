package com.example.thavid.testmvp.chat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.thavid.testmvp.R;

public class MainChatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_chat);
    }
}
