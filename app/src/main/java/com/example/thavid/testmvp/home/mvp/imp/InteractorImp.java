package com.example.thavid.testmvp.home.mvp.imp;

import android.util.Log;
import android.widget.Toast;

import com.example.thavid.testmvp.api.HomeApi;
import com.example.thavid.testmvp.home.callback.Callback;
import com.example.thavid.testmvp.home.callback.HotCallback;
import com.example.thavid.testmvp.home.enity.Hot;
import com.example.thavid.testmvp.home.enity.Popular;
import com.example.thavid.testmvp.home.mvp.Contract;
import com.example.thavid.testmvp.service.Constant;
import com.example.thavid.testmvp.service.ServiceGenerator;

import retrofit2.Call;
import retrofit2.Response;

public class InteractorImp implements Contract.interactor {
    @Override
    public void getPupular(final Callback callback) {
        HomeApi homeApi = ServiceGenerator.createService(HomeApi.class,Constant.authToken);
        Call<Popular> call = homeApi.getPopular(50);
        call.enqueue(new retrofit2.Callback<Popular>() {
            @Override
            public void onResponse(Call<Popular> call, Response<Popular> response) {
                if(response.isSuccessful()){
                    callback.onSuccess(response);
                }else {
                    callback.onFailed("hhghgjhfgh");
                }
            }

            @Override
            public void onFailure(Call<Popular> call, Throwable t) {
                callback.onFailed(t.getMessage());
            }
        });
    }

    @Override
    public void getHot(int limit, int page, final HotCallback callback) {
        HomeApi homeApi = ServiceGenerator.createService(HomeApi.class,Constant.authToken);
        Call<Hot> call = homeApi.getHot(limit,page);
        call.enqueue(new retrofit2.Callback<Hot>() {
            @Override
            public void onResponse(Call<Hot> call, Response<Hot> response) {
                if(response.isSuccessful()){
                    callback.getHotSuccess(response);
                }else {
                    callback.getHotFail(response.body().getMsg());
                }
            }

            @Override
            public void onFailure(Call<Hot> call, Throwable t) {
                callback.getHotFail("OnFailure");
                Log.e("Faill","hz b");
            }
        });
    }
}
