package com.example.thavid.testmvp;

import android.support.v4.app.Fragment;

/**
 * Created by Vandoeurn on 3/24/2018.
 */

public class TabLayoutData {

    private int badgeText;
    private String title;
    private Fragment fragment;
    private int icon;
    private String tabColor;

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public String getTabColor() {
        return tabColor;
    }

    public void setTabColor(String tabColor) {
        this.tabColor = tabColor;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public TabLayoutData(int badgeText, String title, int icon, Fragment fragment) {
        this.badgeText = badgeText;
        this.title = title;
        this.fragment = fragment;
        this.icon = icon;
    }

    public TabLayoutData(int icon, Fragment fragment){
        this.icon = icon;
        this.fragment = fragment;
    }

    public TabLayoutData(int icon){
        this.icon = icon;
    }

    public int getBadgeText() {
        return badgeText;
    }

    public void setBadgeText(int badgeText) {
        this.badgeText = badgeText;
    }

    @Override
    public String toString() {
        return "TabData{" +
                "badgeText=" + badgeText +
                ", title='" + title + '\'' +
                ", fragment=" + fragment +
                ", icon=" + icon +
                ", tabColor='" + tabColor + '\'' +
                '}';
    }
    /*private String title;
    private Fragment fragment;
    private int icon;
    private int notificationCount;

    public TabLayoutData(Fragment fragment, String title, int icon, int notificationCount) {
        this.title = title;
        this.fragment = fragment;
        this.icon = icon;
        this.notificationCount = notificationCount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(int notificationCount) {
        this.notificationCount = notificationCount;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }*/
}
