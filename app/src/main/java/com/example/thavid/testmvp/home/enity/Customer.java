package com.example.thavid.testmvp.home.enity;

public class Customer {
    private int image;
    private String name;
    private String code;
    private String phone;

    public Customer(int image, String name, String code, String phone) {
        this.image = image;
        this.name = name;
        this.code = code;
        this.phone = phone;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
