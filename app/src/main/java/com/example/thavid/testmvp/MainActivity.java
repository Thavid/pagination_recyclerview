package com.example.thavid.testmvp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.thavid.testmvp.home.Home;
import com.example.thavid.testmvp.login.mvp.Contract;
import com.example.thavid.testmvp.login.mvp.implement.PresentorImp;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements Contract.view {
    @BindView(R.id.editUserName)
    EditText userName;
    @BindView(R.id.edPassword)
    EditText password;
    @BindView(R.id.btnLogin)
    Button btnLogin;

    private Contract.presentor presentor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        presentor = new PresentorImp(this);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presentor.login(userName.getText().toString(),password.getText().toString());
            }
        });
    }

    @Override
    public void loading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loginSuccess() {
        //Toast.makeText(this, "Yeah Login ban hz", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this,Home.class);
        startActivity(intent);
    }

    @Override
    public void loginFail() {
        Toast.makeText(this, "Khos hz b ery", Toast.LENGTH_SHORT).show();
    }
}
