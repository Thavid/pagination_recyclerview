package com.example.thavid.testmvp.home.mvp;


import com.example.thavid.testmvp.home.callback.Callback;
import com.example.thavid.testmvp.home.callback.HotCallback;
import com.example.thavid.testmvp.home.enity.Hot;
import com.example.thavid.testmvp.home.enity.Popular;

import retrofit2.Response;

public interface Contract {
    public interface view{
        public void getSuccess(Response<Popular> response);
        public void getFail(String msg);

        public void getHotSuccess(Response<Hot> response);
        public void getHotFailed(String msg);
    }

    public interface presenter{
        public void getPopular();
        public void getHot(int limit,int page);
        public void showkkk();
    }

    public interface interactor {
        public void getPupular(Callback callback);
        public void getHot(int limit,int page,HotCallback callback);
    }
}
