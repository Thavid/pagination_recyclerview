package com.example.thavid.testmvp.home;

import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.thavid.testmvp.R;
import com.example.thavid.testmvp.home.adapter.CustomerAdapter;
import com.example.thavid.testmvp.home.adapter.ProductAdapter;
import com.example.thavid.testmvp.home.enity.Hot;
import com.example.thavid.testmvp.home.enity.HotItem;
import com.example.thavid.testmvp.home.enity.ItemsPopular;
import com.example.thavid.testmvp.home.enity.Popular;
import com.example.thavid.testmvp.home.mvp.Contract;
import com.example.thavid.testmvp.home.mvp.imp.PresenterImp;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class Home extends AppCompatActivity implements Contract.view {
  /* @BindView(R.id.toolbar)
    Toolbar toolbar;*/

    @BindView(R.id.horizontalRecycler)
    RecyclerView productRecycler;
    List<ItemsPopular> listPopular;
    ProductAdapter productAdapter;

    @BindView(R.id.customerRecycler)
    RecyclerView customerRecycler;
    @BindView(R.id.nestedScrollView)
    NestedScrollView nestedScrollView;
    @BindView(R.id.progressbar)
    ProgressBar progressBar;
    List<HotItem> hotItemList;
    CustomerAdapter customerAdapter;
    private PresenterImp presenterImp;
    private int page = 1;
    private int totalPages = 0;
    View view;
    boolean isLoading = false;
    boolean request = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        /*setSupportActionBar(toolbar);*/
        presenterImp = new PresenterImp(this);
        productRecycler.setNestedScrollingEnabled(false);
        view = view;
        listPopular = new ArrayList<>();
        LinearLayoutManager proLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        productRecycler.setLayoutManager(proLayoutManager);
        productAdapter = new ProductAdapter(this,listPopular);
        productRecycler.setAdapter(productAdapter);

        hotItemList = new ArrayList<>();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        customerRecycler.setLayoutManager(layoutManager);
        customerRecycler.setNestedScrollingEnabled(false);
        customerAdapter = new CustomerAdapter(hotItemList,this,customerRecycler);
        customerRecycler.setNestedScrollingEnabled(false);
        customerRecycler.setAdapter(customerAdapter);
        presenterImp.getPopular();
        presenterImp.getHot(15,page);

        nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged()
            {
                View view = (View)nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);

                int diff = (view.getBottom() - (nestedScrollView.getHeight() + nestedScrollView
                        .getScrollY()));
                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) customerRecycler.getLayoutManager();
                if (diff == 0 && request) {
                    // your pagination code
                    progressBar.setVisibility(view.getVisibility());
                    presenterImp.getHot(15,++page);
                    isLoading = false;
                }
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
    @Override
    public void getSuccess(Response<Popular> response) {
        listPopular.addAll(response.body().getItemsPopular());
        productAdapter.notifyDataSetChanged();
    }

    @Override
    public void getFail(String msg) {
        Log.e("Fail Popular",msg);
    }

    @Override
    public void getHotSuccess(final Response<Hot> response) {
        totalPages = response.body().getTotalPage();
        if(page <= totalPages){
           hotItemList.addAll(response.body().getHotItem());
           customerAdapter.notifyDataSetChanged();
           if (isLoading = false){
               progressBar.setVisibility(view.GONE);
           }
           request = true;
           isLoading = false;
       }else {
           request = false;
           isLoading = true;
            progressBar.setVisibility(view.GONE);
       }
    }

    @Override
    public void getHotFailed(String msg) {
        Log.e("Fail",msg);
    }
}
