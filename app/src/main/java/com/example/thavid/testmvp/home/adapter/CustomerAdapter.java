package com.example.thavid.testmvp.home.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.thavid.testmvp.R;
import com.example.thavid.testmvp.home.Home;
import com.example.thavid.testmvp.home.enity.HotItem;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CustomerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<HotItem> listCustomer;
    private Context context;
    int visibleThreHold = 5;
    boolean loading = false;

    private static final int VIEW_ITEM = 1;
    private static final int VIEW_LOADING = 0;

    public CustomerAdapter(List<HotItem> listCustomer, final Context context, RecyclerView recyclerView) {
        this.listCustomer = listCustomer;
        this.context = context;

        //Pagination
        if(recyclerView.getLayoutManager() instanceof LinearLayoutManager){
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
         /*recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    //Get all item in recyclerview
                    int totalItemCount = linearLayoutManager.getItemCount();
                    //Get position of last item
                    int lasVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    //Request data before lastItem - visibleThreHold
                    if(!loading && totalItemCount <= (lasVisibleItem + visibleThreHold)){
                        loading = true;
                        onScroll.onRecyclerViewScroll(context);
                    }
                }
            });*/
           /* nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
                @Override
                public void onScrollChanged()
                {
                    View view = (View)nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);
                    //Get all item in recyclerview
                    int totalItemCount = linearLayoutManager.getItemCount();
                    //Get position of last item
                    int lasVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    int diff = (view.getBottom() - (nestedScrollView.getHeight() + nestedScrollView
                            .getScrollY()));

                    if (diff == 0) {
                        if(!loading && totalItemCount <= (lasVisibleItem + visibleThreHold)){
                            loading = true;
                            Log.e("oooooo","work");
                            onScroll.onRecyclerViewScroll(context);
                        }
                    }
                }
            });*/
        }
    }

    //Pagination and load
    public void onLoaded(){
        loading = false;
    }
    public boolean loading(){
       return loading = true;
    }
    //end pagination

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // Two layout in recyclerview
        RecyclerView.ViewHolder vh = null;
        View view = LayoutInflater.from(context).inflate(R.layout.cartcustomer,viewGroup,false);
        vh = new MyviewHolder(view);
       /* if (i == VIEW_ITEM){
            View view = LayoutInflater.from(context).inflate(R.layout.cartcustomer,viewGroup,false);
            vh = new MyviewHolder(view);
        }else{
            View view = LayoutInflater.from(context).inflate(R.layout.loading_scroll,viewGroup,false);
            vh = new loadingViewHolder(view);
        }*/
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int i) {
        // Check holder between two layout
        if(holder instanceof MyviewHolder){
            MyviewHolder myviewHolder = (MyviewHolder) holder;
            HotItem item = listCustomer.get(i);
            if(item.getImgPath().isEmpty()){
               myviewHolder.imageView.setImageResource(R.drawable.android);
            }else{
                Picasso.get().load(item.getImgPath())
                        .resize(100,100)
                        .into(myviewHolder.imageView);
                /*Glide.with(context)
                        .load(item.getImgPath()) // Remote URL of image.
                        .into(myviewHolder.imageView); //ImageView to set.*/
            }
            myviewHolder.txtName.setText(item.getItemName());
            myviewHolder.txtCode.setText(item.getItemCode());
            myviewHolder.txtPrice.setText(item.getPrice()+"");
        }else if (holder instanceof loadingViewHolder){
            loadingViewHolder loading = (loadingViewHolder) holder;
            loading.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return listCustomer.size();
    }

    //Check type to show Loading
    @Override
    public int getItemViewType(int position) {
        return (listCustomer.get(position) != null) ? VIEW_ITEM : VIEW_LOADING;
    }

    //show loading of pagination
    public void showProgress(){
        listCustomer.add(null);
        notifyItemInserted(listCustomer.size()-1);
    }
    public void hideProgress(){
        if(listCustomer.size() != 0){
            listCustomer.remove(listCustomer.size() - 1);
            notifyItemRemoved(listCustomer.size()-1);
        }
    }
    public class MyviewHolder extends RecyclerView.ViewHolder{
        public ImageView imageView;
        private TextView txtName,txtCode,txtPrice;
        public MyviewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imgLogo);
            txtName = itemView.findViewById(R.id.txtName);
            txtCode = itemView.findViewById(R.id.txtcode);
            txtPrice = itemView.findViewById(R.id.txtPhone);
        }
    }

    public class loadingViewHolder extends RecyclerView.ViewHolder{
        ProgressBar progressBar;
        public loadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }
}
