package com.example.thavid.testmvp.home.mvp.imp;

import android.util.Log;

import com.example.thavid.testmvp.home.callback.Callback;
import com.example.thavid.testmvp.home.callback.HotCallback;
import com.example.thavid.testmvp.home.enity.Hot;
import com.example.thavid.testmvp.home.enity.Popular;
import com.example.thavid.testmvp.home.mvp.Contract;

import retrofit2.Response;

public class PresenterImp implements Contract.presenter {
    private Contract.interactor interactor;
    private Contract.view view;

    public PresenterImp(Contract.view view) {
        this.view = view;
        interactor = new InteractorImp();
    }

    @Override
    public void getPopular() {
        interactor.getPupular(new Callback() {
            @Override
            public void onSuccess(Response<Popular> response) {
                view.getSuccess(response);
            }

            @Override
            public void onFailed(String msg) {
                view.getFail(msg);
            }
        });
    }

    @Override
    public void getHot(int limit, int page) {
        interactor.getHot(limit, page, new HotCallback() {
            @Override
            public void getHotSuccess(Response<Hot> response) {
                view.getHotSuccess(response);
            }

            @Override
            public void getHotFail(String msg) {
                view.getHotFailed(msg);
            }
        });
    }

    @Override
    public void showkkk() {
        Log.e("Hahahaha","KKKKKK");
    }
}
