package com.example.thavid.testmvp.home.enity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Popular {
    private int status;
    private List<String> slideShow;
    private List<ItemsPopular> itemsPopular;
    private String msg;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<String> getSlideShow() {
        return slideShow;
    }

    public void setSlideShow(List<String> slideShow) {
        this.slideShow = slideShow;
    }

    public List<ItemsPopular> getItemsPopular() {
        return itemsPopular;
    }

    public void setItemsPopular(List<ItemsPopular> itemsPopular) {
        this.itemsPopular = itemsPopular;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "Popular{" +
                "status=" + status +
                ", slideShow=" + slideShow +
                ", itemsPopular=" + itemsPopular +
                ", msg='" + msg + '\'' +
                '}';
    }
}
