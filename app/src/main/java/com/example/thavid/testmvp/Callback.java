package com.example.thavid.testmvp;

public interface Callback {
    public void onSuccess();
    public void onFailed();
}
