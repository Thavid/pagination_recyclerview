package com.example.thavid.testmvp.login.mvp.implement;

import com.example.thavid.testmvp.Callback;
import com.example.thavid.testmvp.login.mvp.Contract;

public class PresentorImp implements Contract.presentor {
    private Contract.view view;
    private Contract.interactor interactor;

    public PresentorImp(Contract.view view) {
        this.view = view;
        interactor = new InteractorImp();
    }

    @Override
    public void login(String userName, String password) {
        interactor.login(userName, password, new Callback() {
            @Override
            public void onSuccess() {
                view.loginSuccess();
            }
            @Override
            public void onFailed() {
                view.loginFail();
            }
        });
    }
}
